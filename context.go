// context processing. The heart of JSON-LD.
// TODO: this is mostly the algorithm defined in the JSON-LD api spec. This
// code often looks more like JavaScript than Go, so it needs a general
// refactor once we are sure it works.
// TODO: this package in general currently handles IRIs as URIs, without any
// special care being taken. Some bugs may arise from this, but we need to
// check.

package ld

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

type termDefinition struct {
	IRIMapping       string
	Reverse          bool
	Type             string
	LanguageMapping  string
	ContainerMapping string
}

// context is the Go representation of the JSON-LD context.
//
// Only createTerm and processContext modify the underlying context. All other
// methods access it read-only.
type context struct {
	termDefinitions map[string]termDefinition
	BaseIRI         string
	Vocab           string
	DefaultLanguage string
}

// creates a new context in memory
func (c *context) clone() *context {
	if c == nil {
		return nil
	}
	n := &context{
		termDefinitions: make(map[string]termDefinition, len(c.termDefinitions)),
		BaseIRI:         c.BaseIRI,
		Vocab:           c.Vocab,
		DefaultLanguage: c.DefaultLanguage,
	}
	for k, v := range c.termDefinitions {
		n.termDefinitions[k] = v
	}
	return n
}

// DocumentLoader handles fetching remote documents. The first argument is the
// URI where the resource is located: data should then be parsed by using
// ParseValues, which will transform everything into this package's data
// structure for handling JSON data.
//
// In places where there might be a DocumentLoader and there isn't any, the
// defaultDocumentLoader is used instead. defaultDocumentLoader will fetch
// documents through standard HTTP requests, using JSON-LD's Accept header. It
// will then parse the values and store them in the cache, in case they are
// requested in the future. Cache expires after 24 hours.
type DocumentLoader interface {
	LoadDocument(string) (ValueSlice, error)
}

type processorOptions struct {
	Loader DocumentLoader
}

type processor struct {
	processorOptions
	DocumentIRI   string
	ActiveContext *context
	LocalContexts ValueSlice

	// are only set when processing context through processContext
	currentContext *object
	defined        map[string]bool
}

var (
	errInvalidLocalContext    = errors.New("ld: invalid local context")
	errInvalidBaseIRI         = errors.New("ld: invalid base IRI")
	errInvalidVocabMapping    = errors.New("ld: invalid vocab mapping")
	errInvalidDefaultLanguage = errors.New("ld: invalid default language")
)

// processContext processes the processor's local context, and places the
// resulting context information inside of p.ActiveContext, initializing it if
// is nil.
//
// processContext is loosely based on the Context Processing Algorithm:
// https://www.w3.org/TR/json-ld-api/#context-processing-algorithms . Most of
// the code is based on the algorithm, although we make several modifications.
//
// Before calling processContext, you should set DocumentIRI to be the IRI of
// the document being processed. You should also at least set LocalContexts to
// the value of a @context inside a JSON-LD object.
func (p *processor) processContext(remoteContexts []string) (err error) {
	// initialize current active context if it is nil.
	if p.ActiveContext == nil {
		p.ActiveContext = &context{}
	}
	if p.ActiveContext.termDefinitions == nil {
		p.ActiveContext.termDefinitions = make(map[string]termDefinition)
	}

	vals := p.LocalContexts
	c := p.ActiveContext

	// 2: vals contains the local context, and each single val is a context to
	// be processed.
	for _, val := range vals {
		if val.Kind == kindNull {
			// 3.1: null: local context is asking us to reset the active context
			// Set the context's base IRI to be the IRI of the current document.
			*c = context{BaseIRI: p.DocumentIRI}
			continue
		}
		if val.Kind == kindString {
			// 3.2: string - load from URI
			err = p.loadFromURL(remoteContexts, val.String)
			if err != nil {
				return
			}
			continue
		}

		// carry on only if this is an object.
		if val.Kind != kindObject {
			return errInvalidLocalContext
		}

		// 3.4: parsing of @base
		obj := val.Object
		if base := obj.GetFirst("@base"); base != nil && len(remoteContexts) == 0 {
			switch base.Kind {
			case kindNull:
				// base is null: local context wants us to reset the base URL
				c.BaseIRI = ""
			case kindString:
				// base is a string, thus an IRI: we resolve it. if base is an
				// absolute IRI, then it will be returned as such, otherwise
				// it will be returned resolved against c.BaseIRI
				c.BaseIRI = resolveIRI(c.BaseIRI, val.String)
			default:
				// other type: @base does not have a valid value
				return errInvalidBaseIRI
			}
		}

		// 3.5: parsing of @vocab
		if vocab := obj.GetFirst("@vocab"); vocab != nil {
			if vocab.Kind != kindNull && !strings.Contains(vocab.String, ":") {
				return errInvalidVocabMapping
			}
			// string which is an IRI = set vocab to this
			c.Vocab = vocab.String
		}

		// 3.6: parsing of @language
		if lang := obj.GetFirst("@language"); lang != nil {
			if lang.Kind != kindString && lang.Kind != kindNull {
				return errInvalidDefaultLanguage
			}
			c.DefaultLanguage = strings.ToLower(vals[0].String)
		}

		// set the currentContext of the processor to be this object - this
		// will be needed by createTerm and expandIRI
		p.currentContext = &obj
		p.defined = make(map[string]bool)

		// 3.8: parse the values in the object itself.
		for _, kv := range obj.Mappings {
			if kv.Key == "@base" || kv.Key == "@vocab" || kv.Key == "@language" {
				continue
			}
			err = p.createTerm(kv.Key, kv.Values)
			if err != nil {
				return
			}
		}

		p.currentContext = nil
		p.defined = nil
	}

	return
}

var errRecursiveContext = errors.New("ld: recursive context inclusion")

func (p *processor) loadFromURL(remoteContexts []string, documentURI string) error {
	// get URI by resolving it against the current context's base IRI
	uri := resolveIRI(p.ActiveContext.BaseIRI, documentURI)

	for _, ctx := range remoteContexts {
		if ctx == uri {
			return errRecursiveContext
		}
	}
	remoteContexts = append(remoteContexts, uri)

	if p.Loader == nil {
		p.Loader = &defaultDocumentLoader{
			cache: make(map[string]*cachedObject, 1),
		}
	}

	vals, err := p.Loader.LoadDocument(uri)
	if err != nil {
		return err
	}

	// Iterate over values til we find an object.
	for _, val := range vals {
		if val.Kind != kindObject {
			continue
		}
		// Create a new processor to handle the parsing of the new context.
		// We can thus set the DocumentIRI to be the effective document URI and
		// change the localContext.
		// We still place the ActiveContext to be our own, so that the new terms
		// will be placed under our own.
		newProc := &processor{
			processorOptions: p.processorOptions,
			DocumentIRI:      uri,
			ActiveContext:    p.ActiveContext,
			LocalContexts:    val.Object.Get("@context"),
		}
		err = newProc.processContext(remoteContexts)
		if err != nil {
			return err
		}
		break
	}

	return nil
}

var (
	errCyclicIRIMapping        = errors.New("ld: cyclic IRI mapping")
	errKeywordRedefinition     = errors.New("ld: keyword redefinition")
	errInvalidTermDefinition   = errors.New("ld: invalid term definition")
	errInvalidTypeMapping      = errors.New("ld: invalid type mapping")
	errInvalidReverseProp      = errors.New("ld: invalid reverse property")
	errInvalidIRIMapping       = errors.New("ld: invalid IRI mapping")
	errInvalidKeywordAlias     = errors.New("ld: invalid keyword alias")
	errInvalidContainerMapping = errors.New("ld: invalid container mapping")
	errInvalidLanguageMapping  = errors.New("ld: invalid language mapping")

	keywordMap = map[string]struct{}{
		"@context":   {},
		"@id":        {},
		"@value":     {},
		"@language":  {},
		"@type":      {},
		"@container": {},
		"@list":      {},
		"@set":       {},
		"@reverse":   {},
		"@index":     {},
		"@base":      {},
		"@vocab":     {},
		"@graph":     {},
		":":          {},
	}
	containerKeywordMap = map[string]struct{}{
		"@list":     {},
		"@set":      {},
		"@index":    {},
		"@language": {},
	}
)

func (p *processor) createTerm(termName string, values ValueSlice) (err error) {
	// 1: make sure there are no re-definitions in the current local context
	res, isDefined := p.defined[termName]
	if isDefined {
		if res {
			// already defined
			return
		}
		// res is false: the same term is still being defined in another
		// createTerm up in the stack => cyclic iri mapping
		return errCyclicIRIMapping
	}

	// 2: mark that we're currently defining the term
	if p.defined != nil {
		p.defined[termName] = false
	}

	// 3: make sure we aren't trying to redefine a keyword
	if _, ok := keywordMap[termName]; ok {
		return errKeywordRedefinition
	}

	// 4: remove existing term definitions in the active context
	delete(p.ActiveContext.termDefinitions, termName)

	var obj object
	switch values[0].Kind {
	case kindObject:
		obj = values[0].Object
	case kindString, kindNull:
		// 7: convert strings to {"@id": "str"}
		obj.KeyMap = make(map[string]int, 1)
		(&obj).Set(objectMapping{"@id", ValueSlice{values[0]}, true})
	default:
		// 8: Must be null, string or object.
		return errInvalidTermDefinition
	}

	id := obj.GetFirst("@id")

	// 6: check if value is null or {"@id": null}
	if id != nil && id.Kind == kindNull {
		// TODO: the spec says we should set it to null - although we can't
		// really do that here (unless we change it to be
		// map[string]*termDefinition). At the moment simply leaving it to its
		// zero value doesn't seem to make a difference, will need to check
		// later if this has unintended side effects.
		p.ActiveContext.termDefinitions[termName] = termDefinition{}
		p.defined[termName] = true
		return nil
	}

	var td termDefinition

	// 10: @type
	termType := obj.GetFirst("@type")
	if termType != nil {
		// 10.1
		if termType.Kind != kindString {
			return errInvalidTypeMapping
		}
		// 10.2 and 10.3
		td.Type, err = p.expandIRI(
			termType.String,
			true, false,
		)
		if err != nil {
			return
		}
		// 10.2
		if td.Type != "@id" && td.Type != "@vocab" && !strings.Contains(td.Type, ":") {
			return errInvalidTypeMapping
		}
	}

	// 11: @reverse
	if reverse := obj.GetFirst("@reverse"); reverse != nil {
		switch {
		case id != nil:
			return errInvalidReverseProp
		case reverse.Kind != kindString:
			return errInvalidIRIMapping
		}
		// 11.3
		td.IRIMapping, err = p.expandIRI(
			reverse.String,
			true, false,
		)
		if err != nil {
			return
		}
		if !strings.Contains(td.IRIMapping, ":") {
			return errInvalidIRIMapping
		}
		// 11.4
		if containerVals := obj.Get("@container"); len(containerVals) > 0 {
			container := containerVals[0]
			// if it's a string, container must be @set or @index. Or it can be
			// null.
			if !(container.String == "@set" || container.String == "@index" ||
				container.Kind == kindNull) {
				return errInvalidReverseProp
			}
			td.ContainerMapping = container.String
		}
		td.Reverse = true
		p.ActiveContext.termDefinitions[termName] = td
		p.defined[termName] = true
		return nil
	}

	switch {
	case id != nil && id.String != termName:
		// 13: @id parsing
		// 13.1
		if id.Kind != kindString {
			return errInvalidIRIMapping
		}
		// 13.2
		td.IRIMapping, err = p.expandIRI(
			id.String,
			true, false,
		)
		if err != nil {
			return
		}
		if _, ok := keywordMap[td.IRIMapping]; !ok && !strings.Contains(td.IRIMapping, ":") {
			return errInvalidIRIMapping
		}
		if td.IRIMapping == "@context" {
			return errInvalidKeywordAlias
		}
	case strings.Contains(termName, ":"):
		// 14: local dependency
		colonIndex := strings.Index(termName, ":")
		prefix := termName[:colonIndex]
		// 14.1: dependency found in our local context: let's make sure it gets
		// defined in the activeContext if it can.
		if dep := p.currentContext.Get(prefix); len(dep) > 0 {
			err = p.createTerm(prefix, dep)
			if err != nil {
				return
			}
		}
		// 14.2: we then make sure our activeContext has the dependency, and if
		// it does, we set our own IRI mapping to solved dependency + suffix
		if depDefinition, ok := p.ActiveContext.termDefinitions[prefix]; ok {
			td.IRIMapping = depDefinition.IRIMapping + termName[colonIndex+1:]
		} else {
			// 14.3
			td.IRIMapping = termName
		}
	default:
		// 15: nothing else we can try, we just use our vocabulary mapping.
		if p.ActiveContext.Vocab == "" {
			return errInvalidIRIMapping
		}
		td.IRIMapping = p.ActiveContext.Vocab + termName
	}

	if container := obj.GetFirst("@container"); container != nil {
		td.ContainerMapping = container.String
		if _, ok := containerKeywordMap[td.ContainerMapping]; !ok {
			return errInvalidContainerMapping
		}
	}
	if lang := obj.GetFirst("@language"); lang != nil && id == nil {
		if lang.Kind != kindNull && lang.Kind != kindString {
			return errInvalidLanguageMapping
		}
		td.LanguageMapping = strings.ToLower(lang.String)
	}

	// IT IS DONE
	p.ActiveContext.termDefinitions[termName] = td
	p.defined[termName] = true
	return nil
}

// expandIRI takes an IRI and expands it to its complete form using the
// definitions in the processor's activeContext.
//
// expandIRI only returns errors when called from a processContext: otherwise,
// err is always nil.
func (p *processor) expandIRI(iri string, vocab, documentRelative bool) (string, error) {
	// TODO: according to the spec, `iri` can also be null.
	// Need to determine if we should add iri == "" to the following or make iri
	// a proper *string and add iri == nil.

	// if this is a keyword, we don't need to expand it
	if _, exists := keywordMap[iri]; exists {
		return iri, nil
	}

	// if iri is in our local/active context, then we return its IRI mapping
	if err := p.createTermIfNecessary(iri); err != nil {
		return "", err
	}
	if def, ok := p.ActiveContext.termDefinitions[iri]; ok && vocab {
		return def.IRIMapping, nil
	}

	// If the IRI has a colon, we check if it's composed of prefix + suffix
	// (and the prefix has a key in the Active context)
	if idx := strings.Index(iri, ":"); idx != -1 {
		prefix, suffix := iri[:idx], iri[idx+1:]
		if prefix == "_" || strings.HasPrefix(suffix, "//") {
			return iri, nil
		}
		if err := p.createTermIfNecessary(prefix); err != nil {
			return "", err
		}
		if def, ok := p.ActiveContext.termDefinitions[prefix]; ok {
			return def.IRIMapping + suffix, nil
		}
		return iri, nil
	}

	if vocab && p.ActiveContext.Vocab != "" {
		return p.ActiveContext.Vocab + iri, nil
	}
	if documentRelative {
		return resolveIRI(p.DocumentIRI, iri), nil
	}

	return iri, nil
}

func (p *processor) createTermIfNecessary(term string) error {
	if p.defined == nil {
		return nil
	}
	if vals := p.currentContext.Get(term); len(vals) > 0 && !p.defined[term] {
		err := p.createTerm(term, vals)
		if err != nil {
			return err
		}
	}
	return nil
}

func resolveIRI(baseIRI, refIRI string) string {
	baseURI, err := url.Parse(baseIRI)
	if err != nil {
		return ""
	}
	refURI, err := url.Parse(refIRI)
	if err != nil {
		return ""
	}
	return baseURI.ResolveReference(refURI).String()
}

type cachedObject struct {
	createdAt time.Time
	// mtx guards only v and err: callers can access createdAt if they have a
	// lock on defaultDocumentLoader.
	v   ValueSlice
	err error
	mtx sync.Mutex
}

// defaultDocumentLoader is a document loader which makes standard http requests
// and has a simple caching system for requests done in the previous day.
// Requests not done in the previous 24 hours are considered expired and are
// removed.
//
// defaultDocumentLoader is thread-safe.
type defaultDocumentLoader struct {
	cache map[string]*cachedObject
	mtx   sync.Mutex
}

// static check: *defaultDocumentLoader MUST be a document loader.
var _ = DocumentLoader(&defaultDocumentLoader{})

const acceptHeader = `application/ld+json, application/json;q=0.9, ` +
	`application/javascript;q=0.5, text/javascript;q=0.5, ` +
	`text/plain;q=0.2, */*;q=0.1`

func (d *defaultDocumentLoader) LoadDocument(uri string) (ValueSlice, error) {
	d.mtx.Lock()

	d.removeOldest()

	obj := d.cache[uri]
	if obj != nil {
		d.mtx.Unlock()
		// A bit of magic: if two concurrent LoadDocument happen, the first
		// will fetch the document, and the second will wait for it by
		// requesting the lock.
		obj.mtx.Lock()
		v := obj.v
		err := obj.err
		obj.mtx.Unlock()
		return v, err
	}

	// No previous object, let's create it, store it in the cache and acquire
	// its lock
	obj = &cachedObject{createdAt: time.Now()}
	d.cache[uri] = obj
	obj.mtx.Lock()
	defer obj.mtx.Unlock()

	d.mtx.Unlock()

	// create request to site
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		obj.err = err
		return nil, err
	}
	req.Header.Set("Accept", acceptHeader)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		obj.err = err
		return nil, err
	}

	// create json decoder, parse the values
	defer resp.Body.Close()
	dec := json.NewDecoder(resp.Body)
	dec.UseNumber()
	vals, err := ParseValues(dec)
	if err != nil {
		obj.err = err
		return nil, err
	}

	obj.v = vals

	return vals, nil
}

func (d *defaultDocumentLoader) removeOldest() {
	dayAgo := time.Now().Add(-time.Hour * 24)
	for k, el := range d.cache {
		if el.createdAt.Before(dayAgo) {
			delete(d.cache, k)
		}
	}
}
