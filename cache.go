package ld

import (
	"sync"
	"time"
)

// ContextCacher is an interface for handling caching of Contexts.
// Caching allows for parsing to be much faster in many cases - though it will
// add the overhead of creating a binary representation of the context data and
// creating a hash of it. It has a relatively simple API which could be solved
// by a simple map[uint64]*Object with a RWMutex, though you can set up
// discarding elements after they've reached a certain age.
//
// Implementations are expected to return through Get only objects stored using
// Set. If no object can be found with that ID, nil must be returned.
// Implementations may or may not be thread-safe depending on whether they wish
// to run ParseContext on multiple goroutines using the same ContextCacher.
//
// You can probably find an implementation that suits your needs in
// RolloverCache, as it removes cached elements that have not been used for over
// a certain period of time.
type ContextCacher interface {
	Get(id uint64) *Object
	Set(id uint64, v *Object)
}

// RolloverCache is a simple ContextCacher mechanism. It holds two maps, current
// and next. Next only contains only contexts requested or set over the current
// period, defined by Every and defaulting to 30 minutes. Current holds contexts
// requested in the current and previous period.
type RolloverCache struct {
	Every time.Duration

	mutex   sync.RWMutex
	current map[uint64]*Object
	next    map[uint64]*Object
}

// Get retrieves an object from the RolloverCache. Getting an item that already
// exists inside the "current" map will allow this item to be also placed inside
// the "next" map.
func (t *RolloverCache) Get(id uint64) *Object {
	// lock mutex read-only
	t.mutex.RLock()
	c, ok := t.current[id]
	if !ok {
		// we don't know the context requested - ask caller to fetch it and then
		// call Set
		t.mutex.RUnlock()
		return nil
	}

	// check if next has the requested id
	_, ok = t.next[id]
	t.mutex.RUnlock()

	// next doesn't have it - we need to copy it over.
	if !ok {
		// we don't have to fear assignment to nil map. This branch is only
		// executed if current has returned something different than nil - and
		// current and next are initialized at the same time, so there's no way
		// one is nil and the other is not.
		t.mutex.Lock()
		t.next[id] = c
		t.mutex.Unlock()
	}

	return c
}

// Set sets an object in the RolloverCache.
func (t *RolloverCache) Set(id uint64, o *Object) {
	t.mutex.Lock()
	// Initialization: create maps and goroutine
	if t.next == nil {
		if t.Every <= 0 {
			t.Every = time.Minute * 30
		}
		t.next = make(map[uint64]*Object, 1)
		t.current = make(map[uint64]*Object, 1)
		go t.routine()
	}

	// set item in both next and current
	t.next[id] = o
	t.current[id] = o

	t.mutex.Unlock()
}

func (t *RolloverCache) routine() {
	ticker := time.Tick(t.Every)
	for range ticker {
		t.mutex.Lock()
		t.current = t.next
		// we can expect map size to grow a little, we give a headroom of 4
		t.next = make(map[uint64]*Object, len(t.current)+4)
		t.mutex.Unlock()
	}
}
