package ld

import (
	"strings"
	"testing"
)

func TestMarshal(t *testing.T) {
	emptyCtx, err := Parse(strings.NewReader(`[]`))
	equal(t, err, nil)
	t.Run("Simple", func(t *testing.T) {
		s := struct {
			MyID string `ld:"id"`
		}{"https://toot.cafe"}
		const want = `{"@context":{"id":"@id"},"id":"https://toot.cafe"}`

		ctx, err := Parse(strings.NewReader(want))
		equal(t, err, nil)
		err = ctx.ParseContext()
		equal(t, err, nil)

		res, err := ctx.Marshal(s)
		equal(t, err, nil)
		equal(t, res, []byte(want))
	})
	t.Run("ContainerSlice", func(t *testing.T) {
		s := struct {
			ContainerSlice []int
		}{[]int{1}}
		const want = `{"@context":{"containerSlice":{"@container":"@list","@id":"_:cs"}},"containerSlice":[1]}`

		ctx, err := Parse(strings.NewReader(want))
		equal(t, err, nil)
		err = ctx.ParseContext()
		equal(t, err, nil)

		res, err := ctx.Marshal(s)
		equal(t, err, nil)
		equal(t, res, []byte(want))
	})
	t.Run("IntSlice", func(t *testing.T) {
		s := struct {
			IntSlice []int
		}{[]int{1}}
		const want = `{"intSlice":1}`

		res, err := emptyCtx.Marshal(s)
		equal(t, err, nil)
		equal(t, res, []byte(want))
	})
	t.Run("Embedded", func(t *testing.T) {
		type S1 struct {
			Field string
		}
		s := struct {
			S1
		}{}
		s.Field = ":^)"
		const want = `{"field":":^)"}`

		res, err := emptyCtx.Marshal(s)
		equal(t, err, nil)
		equal(t, res, []byte(want))
	})
	t.Run("Omitempty", func(t *testing.T) {
		s := struct {
			Field  string  `ld:",omitempty"`
			Field2 *string `ld:",omitempty"`
			Field3 int     `ld:",omitempty"`
		}{}
		const want = `{}`

		res, err := emptyCtx.Marshal(s)
		equal(t, err, nil)
		equal(t, res, []byte(want))
	})
	t.Run("NoOmitempty", func(t *testing.T) {
		s := struct {
			Field  string
			Field2 *string
			Field3 int
		}{}
		const want = `{"field":"","field2":null,"field3":0}`

		res, err := emptyCtx.Marshal(s)
		equal(t, err, nil)
		equal(t, res, []byte(want))
	})
	t.Run("Named", func(t *testing.T) {
		type S struct {
			Field string
		}
		s := S{"xd"}
		const want = `{"@type":"S","field":"xd"}`

		res, err := emptyCtx.Marshal(s)
		equal(t, err, nil)
		equal(t, res, []byte(want))
	})
}

func BenchmarkMarshal(b *testing.B) {
	ctx, err := Parse(strings.NewReader(`{"@context":` + mastodonContext + `}`))
	equal(b, err, nil)
	ctx.Loader = commonLoader
	err = ctx.ParseContext()
	equal(b, err, nil)
	type S2 struct {
		Field5 bool
	}
	type S1 struct {
		Field1 string
		Field2 string `ld:",omitempty"`
		Field3 int
		Field4 S2
	}
	s := struct {
		S1
		To           []string
		OrderedItems []int
	}{}
	s.Field1 = "daisuki"
	s.Field4.Field5 = true
	s.OrderedItems = []int{1}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, err := ctx.Marshal(s)
		equal(b, err, nil)
	}
}
